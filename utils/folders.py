import logging
import os

logger = logging.getLogger('webapp.utils.folders')


def create_directory(folder_path):
    """
    Create a folder in the given path
    :param folder_path: the folder path
    :type folder_path: str
    :return:
    """
    try:
        logger.info("Creating directory {}".format(folder_path))
        os.mkdir(folder_path)
        return True
    except FileExistsError:
        logger.info("Path {} already exists".format(folder_path))
    except OSError as e:
        logger.warning("Creation of the directory {} failed ({})".format(folder_path, e))
    return False