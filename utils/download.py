import os
import subprocess

def check_downloaded_file(fp):
    if os.path.isfile(fp):
        if os.path.getsize(fp) > 0:
            return 
        else:
            raise Exception('File %s is empty' % fp)
    else:
        raise Exception('File %s was not created' % fp)

def aria2c(uri, dest):
    dest_dir = os.path.dirname(dest)
    dest_fn = os.path.basename(dest)
    cmd = "aria2c --file-allocation=none --allow-overwrite=true -k 5M -j 16 -x 16 -s 16 --check-certificate=false '%s' -d %s -o %s" \
           %(uri, dest_dir, dest_fn)
    subprocess.check_output(cmd, shell=True)
    check_downloaded_file(dest)

