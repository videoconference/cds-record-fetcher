import datetime
import logging
import os
import time
import html
import requests
import click
import re
from flask.cli import with_appcontext
from lxml import etree
import signal
from bs4 import BeautifulSoup

from utils.folders import create_directory
from utils.download import aria2c 

logger = logging.getLogger("webapp.cds")

class TimeoutException(Exception):
    pass

def __signal_handler(signum, frame):
        raise TimeoutException

signal.signal(signal.SIGALRM, __signal_handler)

@click.command()
@click.option("--start", default=0, help="The first iteration of the loop")
@click.option("--end", default=8600, help="The last iteration of the loop")
@click.option("--year", default="", help="The year of the searched records")
@click.option("--lang", default="", help="The language of the searched records (fre)")
@with_appcontext
def get_cds_records(start, end, year="", lang=""):
    """
    Obtain all the CDS records

    :return: 0 if the script ends well
    :rtype: int
    """
    logger.info("Starting script")

    # 13/01/2022: 405.294 results in english -> 8600 batches
    # 13/01/2022: 35.162 results in french -> 704 batches
    url = "https://cds.cern.ch/search?cc=Articles+%26+Preprints&rg={}&of=xm&jrec={}"

    if lang != "":
        logger.info("Using french language for the requests")
        url += "&p=041__a%3A" + lang

    if year != "":
        logger.info(f"Searching records of the year {year}")
        url += f"&p2={year}&f2=year"

    # 146570 -> 2940 batches
    # url = 'https://cds.cern.ch/search?cc=Books%20%26%20Proceedings&rg={}&of=xm&jrec={}'
    number_items_per_batch = 50
    seconds_between_batches = 1

    count = 0

    if not os.path.exists("results"):
        create_directory(f"results")

    with open("results/records.csv", "w") as myfile:
        myfile.write("")

    for current_page in range(start, end):
        current_iteration = number_items_per_batch + (
            number_items_per_batch * current_page
        )
        new_url = url.format(number_items_per_batch, current_iteration)
        logger.info(f"Requesting {new_url}")
        logger.info(
            f"Running batch {current_page}. start_item: {current_iteration} end_item:{current_iteration + number_items_per_batch}"
        )
        results = requests.get(new_url)
        # utf-8 vs iso-8859-1
        logger.info(f"Encoding: {results.apparent_encoding} and {results.encoding}")
        content = results.content.decode(results.apparent_encoding)
        # logger.info(content)
        root = etree.fromstring(content.encode())

        date = ""
        file_name = ""

        for record in root.getchildren():
            for field in record.getchildren():
                if field.tag == "{http://www.loc.gov/MARC21/slim}controlfield":
                    if field.get("tag", False) == "001":
                        file_name = field.text
                    if field.get("tag", False) == "005":
                        date = field.text

                    if date != "" and file_name != "":
                        break

            month, year = get_simple_record_date(date)
            current_path = f"results/{year}/{month}"
            logger.debug(f"--Iterating record {count} {file_name} ({month}-{year})")
            if not os.path.exists(current_path):
                create_directory(f"results/{year}")
                create_directory(f"results/{year}/{month}")

            file_path = f"{current_path}/{file_name}.xml"
            if not os.path.exists(file_path):
                write_xml_file(file_path, record, encoding=results.encoding)
                add_record_to_file("results/records.csv", file_name, year, month)
                count += 1
            else:
                logger.warning(f"----Record {file_name} already exists")

        time.sleep(seconds_between_batches)
    return 0



@click.command()
@click.option("--csv", default="", help="CSV file to manage records database")
@click.option("--from-date", default="", help="From date (yyyy/mm/dd)")
@click.option("--to-date", default="", help="To date (yyyy/mm/dd)")
@click.option("--output-dir", default="./dump", help="Output dir. Default: ./dump")
@click.option("--category", default="", help="Dump specific category")
@click.option("--start-page", default=0, help="Start page. Requires --category option. Default: 0")
@click.option("--use-aria2c", default=False, help="Use aria2c to download documents. Default: false")
@with_appcontext
def mllp_autotraining_cds_dump(csv="", from_date="", to_date="", output_dir="", start_page=0, category="", use_aria2c=False):

    category_list = [
                  "Articles+%26+Preprints", "Books+%26+Proceedings", 
                  "IPPOG+Documents", "Progress+Report", "CERN+Series", 
                  "CERN+Departments", "CERN+Accelerators",
                  "CERN+Experiments", "CERN+R%26D+Projects"
                 ]

    if category != "":
        categories = [(category, start_page)]
    else:
        categories = [(x, 0) for x in category_list ]

    items_per_page = 100
    seconds_between_batches = 1
    request_attempts = 3
    seconds_between_attempts = 5
    seconds_between_categories = 5

    request_timeout = 30
    download_timeout = 300

    logger.info("Starting script")


    ids = {}
    if csv != "" and os.path.exists(csv):
        logger.info(f"Loading csv {csv}")
        with open(csv) as fd:
            for l in fd:
                ids[l.strip().split(";")[0]] = None
        logger.info(f"Loaded {len(ids.keys())} IDs.")

    if not os.path.exists(output_dir):
        create_directory(output_dir)

    for category, page in categories:

        url = f'https://cds.cern.ch/search?ln=en&cc={category}'
    
        count = 0
    
        if from_date != "":
            start_date = datetime.datetime.strptime(from_date, "%Y/%m/%d")
            logger.info(f"Searching records of category {category} from {from_date}")
            url += f"&d1d={start_date.day}&d1m={start_date.month}&d1y={start_date.year}"
        if to_date != "":
            end_date = datetime.datetime.strptime(to_date, "%Y/%m/%d")
            logger.info(f"Searching records of category {category} up to {to_date}")
            url += f"&d2d={end_date.day}&d2m={end_date.month}&d2y={end_date.year}"
   

        # First scrap first HTML page to check if there are results (if not, search engine will return ALL results...)
        soup = None
        for attempt in range(request_attempts):
            try:
                signal.alarm(request_timeout)
                results = requests.get(url)
                signal.alarm(0)
                # logger.info(f"Encoding: {results.apparent_encoding} and {results.encoding}")
                content = results.content.decode(results.apparent_encoding)
                soup = BeautifulSoup(results.content, features='lxml') 
            except Exception as err:
                sleep_time = (attempt + 1) * seconds_between_attempts
                logger.info(f"There was a problem retrieving or parsing response from {url}: '{err}'. Retrying in {sleep_time} seconds...")
                time.sleep( (attempt + 1) * seconds_between_attempts)
            else:
                break
           
        if soup == None:
            logger.info(f"Giving up with retrieving {url}. Skiping...")
            continue

        if soup.find(text=re.compile("No match within your time limits, discarding this condition...")):
            logger.info(f"This search ({url}) produced 0 results. Skiping...")
            continue

        # Ok, let's scrap them all
        url += "&of=xm" # get XML format
    
        loop = True
        offset = page*items_per_page + 1 - items_per_page # init offset
        current_page = page
        while loop:
            offset += items_per_page
            current_page += 1
            new_url = url + f'&rg={items_per_page}&jrec={offset}'
            logger.info(f"Requesting {new_url}")
            logger.info(
                f"Running category {category}, page {current_page}. start_item: {offset} end_item:{offset + items_per_page}"
            )
            root = None
            for attempt in range(request_attempts):
                try:
                    signal.alarm(request_timeout)
                    results = requests.get(new_url)
                    signal.alarm(0)
                    # logger.info(f"Encoding: {results.apparent_encoding} and {results.encoding}")
                    content = results.content.decode(results.apparent_encoding)
                    root = etree.fromstring(content.encode())
                except Exception as err:
                    sleep_time = (attempt + 1) * seconds_between_attempts
                    logger.info(f"There was a problem retrieving or parsing response from {new_url}: '{err}'. Retrying in {sleep_time} seconds...")
                    time.sleep( (attempt + 1) * seconds_between_attempts)
                else:
                    break
               
            if root == None:
                logger.info(f"Giving up with retrieving {new_url}. Skiping...")
                continue
    
            loop = (len(root.getchildren()) > 0)
    
            for record in root.getchildren():
    
                date = ""
                file_name = ""
                abstract = ""
    
                for field in record.getchildren():
                    if field.tag == "{http://www.loc.gov/MARC21/slim}controlfield":
                        if field.get("tag", False) == "001":
                            file_name = field.text
                        if field.get("tag", False) == "005":
                            date = field.text
                    elif field.tag == "{http://www.loc.gov/MARC21/slim}datafield":
                        if field.get("tag", False) == "520":
                            for subfield in field:
                                if subfield.get("code", False) == "a":
                                     abstract = subfield.text
                                     break
    
                    if date != "" and file_name != "" and abstract != "":
                        break
   
                if file_name in ids:
                    logger.warning(f"----Record {file_name} already exists")
                    continue

                dc_data = None
                try:
                    dc_data, month, year = get_dc_date(file_name)
                except:
                    logger.warning(f"Could not get DC date! Falling back to MARC21 005 field")
                    month, year = get_simple_record_date(date)

                current_path = f"{output_dir}/{year}/{month:02d}/{file_name}"
                if not os.path.exists(current_path):
                    create_directory(f"{output_dir}/{year}")
                    create_directory(f"{output_dir}/{year}/{month:02d}")
                    create_directory(f"{output_dir}/{year}/{month:02d}/{file_name}")
    
                file_path = f"{current_path}/{file_name}.xml"
                if os.path.exists(file_path):
                    logger.warning(f"----Record {file_name} already exists")
                    continue

                logger.info(f"Saving {file_name} XML file to {file_path}")
                write_xml_file(file_path, record, encoding=results.encoding)

                if dc_data != None:
                    dc_file_path = f"{current_path}/{file_name}.dc"
                    logger.info(f"Saving {file_name} DC file to {dc_file_path}")
                    write_dc_file(dc_file_path, dc_data)

                if abstract != "" and abstract != None:
                    file_path_abstract = f"{current_path}/{file_name}.abs.txt"
                    logger.info(f"Saving {file_name} abstract to {file_path_abstract}")
                    write_abstract(file_path_abstract, abstract, encoding=results.encoding)
                els = record.findall(
                    './{http://www.loc.gov/MARC21/slim}datafield[@tag="856"]'
                )
                if els:
                    doc_count = 1
                    for sele in els:
                        try:
                            sub_el = sele.findall(
                                './{http://www.loc.gov/MARC21/slim}subfield[@code="u"]'
                            )[0]
                        except:
                            continue

                        if sub_el is not None:
                            url_pdf = sub_el.text
                            extension = ""
                            if re.match(r".*\.(pdf|pdfa|ppt|pptx|doc|docx)$", url_pdf):
                                try:
                                    extension = os.path.splitext(url_pdf)[1]
                                except:
                                    extension = ".pdf"
                                if extension not in (".pdf",".pdfa",".ppt",".pptx",".doc",".docx"):
                                    extension = ".pdf"

                                logger.info(f"Downloading document no. {doc_count}: {url_pdf} ...")

                                file_path_document = f"{current_path}/{file_name}.doc_{doc_count:02d}{extension}"

                                success = False
                                for attempt in range(request_attempts):

                                    try:
                                        signal.alarm(download_timeout)
                                        if use_aria2c:
                                            aria2c(url_pdf, file_path_document)
                                        else:
                                            r = requests.get(sub_el.text)
                                            r.raise_for_status()
                                        signal.alarm(0)

                                    except requests.exceptions.HTTPError as err:
                                        if err.response.status_code == 404: # file not found
                                            logger.info(f"HTTP 404 error: {err}. Skipping...")
                                            break
                                        else:
                                            sleep_time = (attempt + 1) * seconds_between_attempts
                                            logger.info(f"There was a problem retrieving {url_pdf}: '{err}'. Retrying in {sleep_time} seconds...")
                                            time.sleep( (attempt + 1) * seconds_between_attempts)

                                    except TimeoutException:
                                        logger.info(f"Timed out when downloading {url_pdf}. Skipping...")
                                        break

                                    except Exception as err:
                                        sleep_time = (attempt + 1) * seconds_between_attempts
                                        logger.info(f"There was a problem retrieving {url_pdf}: '{err}'. Retrying in {sleep_time} seconds...")
                                        time.sleep( (attempt + 1) * seconds_between_attempts)

                                    else:
                                        logger.info(f'Success downloading document no. {doc_count}: {url_pdf} ---> {file_path_document}')
                                        success = True
                                        break

                                if success:
                                    if not(use_aria2c):
                                        with open(file_path_document, "wb") as f:
                                            logger.info(f"Saving {file_name} {extension} document to {file_path_document}")
                                            f.write(r.content)
                                    else:
                                        logger.info(f"Saved {file_name} {extension} document to {file_path_document}")
                                    doc_count += 1
                                else:
                                    logger.info(f"Giving up with {url_pdf}. Skipping...")
                            else:
                                #logger.info(
                                #    f"{url_pdf} looks not a pdf file, let's try a different on for {file_name}"
                                #)
                                next
                else:
                    logger.info(f"record {file_name} has no documents")

                if csv != "":
                    with open(csv, "a") as fd:
                        fd.write(f"{file_name};{year};{month};\n")

                ids[file_name] = None

                count += 1
    
            time.sleep(seconds_between_batches)

        logger.info(f"Done with caregory {category}.")
        time.sleep(seconds_between_categories) 

    return 0



@click.command()
@click.option("--from-date", default="", help="From date (yyyy/mm/dd)")
@click.option("--to-date", default="", help="To date (yyyy/mm/dd)")
@click.option("--start-page", default=0, help="Start page (default 0)")
@click.option("--output-dir", default="./dump", help="Output dir. Default: ./dump")
@with_appcontext
def mllp_autotraining_cern_news_dump(from_date="", to_date="", output_dir="", start_page=0):

    seconds_between_batches = 1
    request_attempts = 3
    seconds_between_attempts = 20

    request_timeout = 30
    download_timeout = 120

    logger.info("Starting script")

    url = "https://home.cern/news?title=&topic=All&type=All&audience=All"

    if from_date != "":
        start_date = datetime.datetime.strptime(from_date, "%Y/%m/%d")
        fdate = datetime.datetime.strftime(start_date, '%Y-%m-%d')
        logger.info(f"Searching news from {fdate}")
        url += f"&date_from={fdate}"
    if to_date != "":
        end_date = datetime.datetime.strptime(to_date, "%Y/%m/%d")
        fdate = datetime.datetime.strftime(end_date, '%Y-%m-%d')
        logger.info(f"Searching news up to {fdate}")
        url += f"&date_to={fdate}"


    if not os.path.exists(output_dir):
        create_directory(output_dir)

    loop = True
    current_page = start_page
    while loop:
        new_url = url + f'&page={current_page}'
        logger.info(f"Running CERN News, page {current_page}.")
        logger.info(f"Requesting {new_url}")
        soup = None
        for attempt in range(request_attempts):
            try:
                signal.alarm(request_timeout)
                ret = requests.get(new_url)
                signal.alarm(0)
                soup = BeautifulSoup(ret.content, features='lxml')
            except Exception as err:
                sleep_time = (attempt + 1) * seconds_between_attempts
                logger.info(f"There was a problem retrieving or parsing response from {new_url}: '{err}'. Retrying in {sleep_time} seconds...")
                time.sleep( (attempt + 1) * seconds_between_attempts)
            else:
                break
           
        if soup == None:
            logger.info("Giving up with retrieving page {new_url}. Skiping...")
            continue

        loop = False
        for div in soup.find_all("div", class_="preview-list-component"):
            loop = True
            a = div.find("h3", class_="preview-list-title").find("a")
            if a == None: continue
            art_rel_url = a.get("href")
            file_name = os.path.basename(art_rel_url)
            art_url = "https://home.cern%s" % art_rel_url

            logger.info(f"Requesting article {art_url}")

            art_soup = None
            for attempt in range(request_attempts):
                try:
                    signal.alarm(request_timeout)
                    art_ret = requests.get(art_url)
                    signal.alarm(0)
                    art_soup = BeautifulSoup(art_ret.content, features='lxml')
                except Exception as err:
                    sleep_time = (attempt + 1) * seconds_between_attempts
                    logger.info(f"There was a problem retrieving or parsing response from {art_url}: '{err}'. Retrying in {sleep_time} seconds...")
                    time.sleep( (attempt + 1) * seconds_between_attempts)
                else:
                    break
    
            if art_soup == None:
                logger.info("Giving up with retrieving article {art_url}. Skiping...")
                continue

            date = "1970-01-01"
            try:
                date = art_soup.find("p", class_="news-node-full-content-created_date").find("time").get("datetime").split("T")[0]
            except:
                logger.info("Could not get date from article!")

            year = 1970
            month = 1
            try:
                year = int(date.split("-")[0])
                month = int(date.split("-")[1])
            except:
                logger.info("Cannot extract year and month from date!")

            text = ""

            ## Series article
            if art_url.find("/series/") != -1:
                logger.info("Is a series article!")
                try:
                    t_title = art_soup.find("h3", class_="header-block__name").get_text("").strip()
                    text += f"{t_title}\n\n"
                except:
                    logger.info("Cannot extract title!")
                    pass

                t_body = ""
                for div in art_soup.find_all("div", class_="text-component-text cern_full_html"):
                    for p in div.find_all("p"):
                        pt = p.get_text("").strip()
                        if len(re.sub("_", "", pt)) == 0:
                            continue
                        if pt != "":
                            t_body = "%s%s\n\n" % (t_body, pt)
    
                if t_body.strip() == "":
                     logger.info("Empty contents for this article! Skipping")
                     continue

                text += t_body
                text = text.strip()
                
            ## Regular news article
            else:
                logger.info("Is a regular article.")
                try:
                    t_title = art_soup.find("h1", class_="news-node-full-content-title").get_text("").strip()
                    text += f"{t_title}\n\n"
                except:
                    logger.info("Cannot extract title!")
                    pass
    
                try:
                    t_subtitle = art_soup.find("p", class_="news-node-full-content-strap").get_text("").strip()
                    text += f"{t_subtitle}\n\n"
                except:
                    pass
        
                main_div = art_soup.find("div", class_="news-node-full-content-body")
                if main_div == None:
                     logger.info("Empty contents for this article! Skipping...")
                     continue
    
                t_body = ""
                for p in main_div.find_all("p"):
                    pt = p.get_text("").strip()
                    if len(re.sub("_", "", pt)) == 0:
                        continue
                    if pt != "":
                        t_body = "%s%s\n\n" % (t_body, pt)
    
                if t_body.strip() == "":
                     logger.info("Empty contents for this article! Skipping")
                     continue
        
                text += t_body
                text = text.strip()

            current_path = f"{output_dir}/{year}/{month:02d}"
            if not os.path.exists(current_path):
                create_directory(f"{output_dir}/{year}")
                create_directory(f"{output_dir}/{year}/{month:02d}")

            file_path = f"{current_path}/{file_name}.txt"
            if not os.path.exists(file_path):
                logger.info(f"Saving new object {file_name} to {file_path}")
                write_abstract(file_path, text)
            else:
                logger.info(f"Already exists: {file_path}")

        current_page += 1
        time.sleep(seconds_between_batches)

    return 0




@click.command()
@click.option("--start", default=0, help="The first iteration of the loop")
@click.option("--end", default=8600, help="The last iteration of the loop")
@click.option("--year", default="", help="The year of the searched records")
@with_appcontext
def get_cds_thesis_and_pdf(start, end, year=""):
    logger.info("Starting script")

    # 13/01/2022: 405.294 results in english -> 8600 batches
    # 13/01/2022: 35.162 results in french -> 704 batches
    # url = 'https://cds.cern.ch/search?cc=Articles+%26+Preprints&rg={}&of=xm&jrec={}'
    # url = 'https://cds.cern.ch/search?c=CERN+Theses&op1=a&p=Centre+National+de+la+Recherche+Scientifique+%7C+CNRS+%7C+FR&action_search=Buscar&of=xm&ln=en&rg={}&jrec={}&m1=a&sc=1&so=d&cc=CERN+Theses'
    url = "https://cds.cern.ch/search?c=CERN+Theses&op1=a&p=Centre+National+de+la+Recherche+Scientifique+%7C+CNRS+%7C+FR&action_search=Buscar&of=xm&ln=en&rg=205&m1=a&sc=0&so=d&cc=CERN+Theses"

    # 146570 -> 2940 batches
    # url = 'https://cds.cern.ch/search?cc=Books%20%26%20Proceedings&rg={}&of=xm&jrec={}'
    number_items_per_batch = 50
    seconds_between_batches = 1

    count = 0

    if year != "":
        logger.info(f"Searching records of the year {year}")
        url += f"&p2={year}&f2=year"

    if not os.path.exists("results"):
        create_directory("results")

    with open("results/records.csv", "w") as myfile:
        myfile.write("")

    for current_page in range(start, end):
        current_iteration = number_items_per_batch + (
            number_items_per_batch * current_page
        )
        new_url = url.format(number_items_per_batch, current_iteration)
        logger.info(f"Requesting {new_url}")
        logger.info(
            f"Running batch {current_page}. start_item: {current_iteration} end_item:{current_iteration + number_items_per_batch}"
        )
        results = requests.get(new_url)
        # utf-8 vs iso-8859-1
        logger.info(f"Encoding: {results.apparent_encoding} and {results.encoding}")
        content = results.content.decode(results.apparent_encoding)
        # logger.info(content)
        root = etree.fromstring(content.encode())

        date = ""
        file_name = ""

        for record in root.getchildren():
            # print(record.tag)
            for field in record.getchildren():
                # print(field.attrib)
                # print(field.tag)
                if field.tag == "{http://www.loc.gov/MARC21/slim}controlfield":
                    if field.get("tag", False) == "001":
                        file_name = field.text
                    if file_name != "":
                        break

            current_path = f"results/"

            file_path = f"{current_path}/{file_name}.xml"
            if not os.path.exists(file_path):
                write_xml_file(file_path, record, encoding=results.encoding)
                els = record.findall(
                    './{http://www.loc.gov/MARC21/slim}datafield[@tag="856"]'
                )
                if els:
                    for sele in els:
                        logger.info(f"{file_name} has thesis ")

                        sub_el = sele.findall(
                            './{http://www.loc.gov/MARC21/slim}subfield[@code="u"]'
                        )[0]
                        if sub_el is not None:
                            url_pdf = sub_el.text
                            success = False
                            if re.match(r".*\.(pdf|pdfa)$", url_pdf):
                                logger.info(f"{url_pdf} downloading..")
                                try:
                                    r = requests.get(sub_el.text)
                                    r.raise_for_status()
                                except requests.exceptions.HTTPError as http_err:
                                    logger.warn(f"HTTP error occurred: {http_err}")
                                except Exception as err:
                                    logger.warn(f"Other error occurred: {err}")
                                else:
                                    logger.info(
                                        f'Success - status code: {r.status_code} content-type: {r.headers["content-type"]} encoding: {r.encoding}'
                                    )
                                    success = True
                                if success:
                                    with open(f"./results/{file_name}.pdf", "wb") as f:
                                        f.write(r.content)
                                    break
                            else:
                                logger.info(
                                    f"{url_pdf} looks not a pdf file, let's try a different on for {file_name}"
                                )
                                next
                else:
                    logger.info(f"record {file_name} has no thesis")
                add_thesis_to_file(
                    "results/records.csv", file_name, f"{file_name}.pdf", url_pdf
                )
                count += 1
            else:
                logger.warning(f"----Record {file_name} already exists")

        time.sleep(seconds_between_batches)
    return 0


def get_simple_record_date(string):
    year = 0
    month = 0
    try:
        date = datetime.datetime.strptime(string[0:8], "%Y%m%d")
        year = date.year
        month = date.month
    except ValueError as e:
        logger.error(e)
    return month, year

def get_dc_date(rid):
    sleeps = [ 10, 30, 60 ]
    
    url = f"https://cds.cern.ch/record/{rid}/export/xd"

    for attempt in range(len(sleeps)):
        ret = None

        logger.info(f"Retrieving DC register (attempt {attempt+1}): {url}")
        try:
            ret = requests.get(url)
        except:
            time.sleep( sleeps[attempt] )
        else:
            break
    
    if ret == None:
        raise Exception("Could not get DC file")
    
    root = etree.fromstring(ret.content)

    month = None; year = None;
    for field in root[0]:
        if field.tag == "{http://purl.org/dc/elements/1.1/}date":
            d = field.text.split("T")[0]
            ds = d.split("-")
            if len(ds) == 1:
                year = int(ds[0])
                month = 0
            else:
                year = int(ds[0])
                month = int(ds[1])
            break

    if month == None:
        raise Exception("Could not get DC date")

    return ret.content, month, year
    

def write_abstract(file_path, text, encoding="utf-8"):
    """
    Write abstract text on the file_path specified
    :param file_path: Path of the file that will be written
    :type file_path: str
    :param text: The abstract that will be written
    :type text: str
    :return: True if ok
    :rtype: bool
    """
    if not os.path.exists(file_path):
        with open(file_path, "w") as file:
            file.write(text)
        return True

def write_dc_file(file_path, content):
    if not os.path.exists(file_path):
        with open(file_path, "wb") as file:
            file.write(content)
        return True

def write_xml_file(file_path, record, encoding="utf-8"):
    """
    Write the record on the file_path specified
    :param file_path: Path of the file that will be written
    :type file_path: str
    :param record: The record that will be written
    :type record: pymarc.Record
    :return: True if ok
    :rtype: bool
    """
    if not os.path.exists(file_path):
        with open(file_path, "w") as file:
            content = etree.tostring(
                record, pretty_print=True, encoding=encoding
            ).decode(encoding)
            file.write(html.unescape(content))
        return True


def add_record_to_file(file_path, record_name: str, year: str, month: str):
    """
    Write the record on the file_path specified
    :param file_path: Path of the file that will be written
    :type file_path: str
    :param record: The record that will be written
    :type record: pymarc.Record
    :return: True if ok
    :rtype: bool
    """
    with open(file_path, "a") as file:
        file.write(f"{year},{month},{record_name},{datetime.datetime.now()}\n")
        return True


def add_thesis_to_file(file_path, record_name: str, thesis_file: str, url_pdf: str):
    """
    Write the record on the file_path specified
    :param file_path: Path of the file that will be written
    :type file_path: str
    :param thesis_file: The record that will be written
    :type thesis_file: str
    :param url_pdf: location of PDF in Internet
    :type url_pdf: str
    :return: True if ok
    :rtype: bool
    """
    with open(file_path, "a") as file:
        file.write(f"{record_name},{thesis_file}, {url_pdf}\n")
        return True



