# CDS MLLP CDS Fetch Application

This application fetches the records from CDS and stores their metadata in the `results` folder.

The data stored will be `<YEAR>/<MONTH>/<ID>.xml` and it will be on Marc21 format.

## Setup the environment

```bash
python3 -m venv ./env
source ./venv/bin/activate
pip install -r requirements.txt
```

## Run the application

To run the script:
```bash
FLASK_APP=app.py flask get-cds-records --start=0 --end=100
FLASK_APP=app.py flask get-cds-records --start=0 --end=100 --lang=fre

```

To get documentation:
```bash
FLASK_APP=app.py flask get-cds-records --help
```

To get records and thesis:

```bash
FLASK_APP=app.py flask get-cds-thesis-and-pdf --start 0 --end 20
```

To dump PDFs and abstracts from many categories for autotraining:

```bash
FLASK_APP=app.py flask mllp-autotraining-cds-dump --from-date 2023/01/01 --to-date 2023/12/31 --output-dir /path/to/store/dump
```

To dump CERN News articles (in english) for autotraining:

```bash
FLASK_APP=app.py flask mllp-autotraining-cern-news-dump --from-date 2023/01/01 --to-date 2023/12/31 --start-page 0 --output-dir /path/to/store/dump
```
