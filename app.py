import logging

from flask import Flask

from services.cds import get_cds_records, get_cds_thesis_and_pdf, \
                         mllp_autotraining_cds_dump, mllp_autotraining_cern_news_dump
from utils.logger import setup_logs

app = Flask(__name__)

app.config['LOG_LEVEL'] = 'DEV'
setup_logs(app, 'webapp', to_file=True)
logger = logging.getLogger('webapp')


@app.route('/')
def hello_world():
    return 'Hello, World!'


#
# Add the Command Line commands
#
app.cli.add_command(get_cds_records)
app.cli.add_command(get_cds_thesis_and_pdf)
app.cli.add_command(mllp_autotraining_cds_dump)
app.cli.add_command(mllp_autotraining_cern_news_dump)
